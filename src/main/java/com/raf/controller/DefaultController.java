package com.raf.controller;

import com.raf.forms.BookingForm;
import com.raf.model.FlightReservation;
import com.raf.model.User;
import com.raf.repository.FlightRepository;
import com.raf.repository.FlightReservationRepository;
import com.raf.repository.UserRepository;
import com.raf.service.BookingService;
import com.raf.service.FlightReservationService;
import com.raf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Controller
public class DefaultController {

    @Autowired
    private FlightRepository flightRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private FlightReservationRepository flightReservationRepository;
    @Autowired
    private FlightReservationService flightReservationService;
    @Autowired
    private BookingService bookingService;

    @RequestMapping("/register")
    @ResponseBody
    public Object register(@Valid @ModelAttribute User user, BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            return userService.createUserAccount(user);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping("/booking")
    @ResponseBody
    public Object booking(@Valid BookingForm bookingForm, BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            return bookingService.bookFlight(bookingForm, new Date());
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping("/user")
    @ResponseBody
    public Object user() {
        List<User> users = userRepository.findAll();
        return users;
    }

    @RequestMapping("/flight")
    @ResponseBody
    public Object flight(@RequestParam Date flyDateFrom,
                         @RequestParam Date flyDateTo,
                         @RequestParam String fromPlace,
                         @RequestParam String toPlace) {
        return flightRepository.findAllByFlyDateBetweenAndFromCityAndToCity(flyDateFrom, flyDateTo, fromPlace, toPlace);
    }

    @RequestMapping("/flights")
    @ResponseBody
    public Object flights() {
        return flightRepository.findAll();
    }

    @RequestMapping("/user-flight")
    @ResponseBody
    public Object userFlight(@RequestParam Long id) {
        return flightReservationRepository.findByUserId(id);
    }

    @RequestMapping("/guest-flight")
    @ResponseBody
    public Object guestFlight(@RequestParam Long id) {
        return flightReservationRepository.findByGuestId(id);
    }

    @RequestMapping("/check-flight-reservation")
    @ResponseBody
    public Object checkFlightReservation(@RequestParam Long id) {
        return flightReservationRepository.findOne(id);
    }

    @RequestMapping("/cancel-flight-reservation")
    @ResponseBody
    public Object cancelFlightReservation(@RequestParam Long id) {
        return flightReservationService.cancelReservation(id, LocalDateTime.now().plusDays(3));
    }

    @RequestMapping("/update-flight-reservation")
    @ResponseBody
    public Object updateFlightReservation(@RequestParam Long id, @RequestParam int bags,@RequestParam(required = false,defaultValue = "false") boolean paid) {
        return flightReservationService.modifyReservation(id, LocalDateTime.now().plusDays(2), bags,paid);
    }

    @InitBinder
    public void dataBinding(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
}
