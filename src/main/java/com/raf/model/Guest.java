package com.raf.model;

import com.raf.forms.BookingForm;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Entity for holding data for non registered users
 */
@Entity
@Table(name = "guest")
public class Guest {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String username;
    @NotNull
    private String pesel;
    @NotNull
    @Email
    private String email;

    public Guest() {
    }

    public Guest(BookingForm bookingForm) {
        this.username = bookingForm.getUsername();
        this.pesel = bookingForm.getPesel();
        this.email = bookingForm.getEmail();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
