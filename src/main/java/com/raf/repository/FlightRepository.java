package com.raf.repository;

import com.raf.model.Flight;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface FlightRepository extends JpaRepository<Flight, Long> {
    List<Flight> findAllByFlyDateBetweenAndFromCityAndToCity(Date from, Date to, String fromCity, String toCity);
}
