package com.raf.repository;

import com.raf.model.FlightReservation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;


public interface FlightReservationRepository extends JpaRepository<FlightReservation, Long> {
    List<FlightReservation> findByUserId(Long id);

    List<FlightReservation> findByGuestId(Long id);

    List<FlightReservation> findAllByPaidFalseAndFlightFlyDateBefore(Date date);
}
