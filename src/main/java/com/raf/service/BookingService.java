package com.raf.service;

import com.raf.forms.BookingForm;
import com.raf.model.Flight;
import com.raf.model.FlightReservation;
import com.raf.model.Guest;
import com.raf.model.User;

import java.time.LocalDateTime;
import java.util.Date;


public interface BookingService {
    boolean validateCanUpdateFlightReservation(FlightReservation flightReservation, LocalDateTime nowDateTime);

    boolean validateCanCancelFlightReservation(FlightReservation flightReservation, LocalDateTime nowDateTime);

    boolean discountQualifier(User user, Flight flight);

    Object bookFlight(BookingForm bookingForm,Date reservationDate);

    FlightReservation bookForUser(User user, Flight flight, int bags, Date reservationDate);

    FlightReservation bookForGuest(Guest guest, Flight flight, int bags, Date reservationDate);

}
