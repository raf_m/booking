package com.raf.service;

import com.raf.model.FlightReservation;
import com.raf.repository.FlightReservationRepository;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.MissingResourceException;

@Service
public class FlightReservationServiceImpl implements FlightReservationService {

    @Autowired
    private FlightReservationRepository flightReservationRepository;
    @Autowired
    private BookingService bookingService;

    @Override
    public boolean cancelReservation(long id, LocalDateTime thresholdDate) {
        Validate.notNull(thresholdDate);
        FlightReservation flightReservation = flightReservationRepository.findOne(id);
        if (flightReservation == null) {
            throw new IllegalStateException("Missing FlightReservation id: " + id);
        }
        if (bookingService.validateCanCancelFlightReservation(flightReservation, thresholdDate)) {
            flightReservationRepository.delete(id);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public FlightReservation modifyReservation(long id, LocalDateTime thresholdDate, int bags,boolean paid) {
        Validate.notNull(thresholdDate);
        FlightReservation flightReservation = flightReservationRepository.findOne(id);
        if (flightReservation == null) {
            throw new IllegalStateException("Missing FlightReservation id: " + id);
        }
        if (bookingService.validateCanCancelFlightReservation(flightReservation, thresholdDate)) {
            flightReservation.setBags(bags);
            flightReservation.setPaid(paid);
            return flightReservationRepository.saveAndFlush(flightReservation);
        } else {
            return null;
        }
    }

    @Override
    public long deleteUnpaidFlightReservations(Date thresholdDate) {
        List<FlightReservation> flightReservationList = flightReservationRepository.findAllByPaidFalseAndFlightFlyDateBefore(thresholdDate);
        flightReservationRepository.delete(flightReservationList);
        return flightReservationList.size();
    }
}
