package com.raf.service;

import com.raf.model.Role;
import com.raf.model.RoleType;
import com.raf.model.User;
import com.raf.repository.RoleRepository;
import com.raf.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.raf.model.RoleType.*;
import static com.raf.model.RoleType.vip;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public User createUserAccount(User user) {
        if (user.isVip()) {
            Role vip_role = roleRepository.findByName(vip);
            user.getRoles().add(vip_role);
        }
        return userRepository.saveAndFlush(user);
    }
}
