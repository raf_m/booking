package com.raf.service;

import com.raf.forms.BookingForm;
import com.raf.model.*;
import com.raf.repository.FlightRepository;
import com.raf.repository.FlightReservationRepository;
import com.raf.repository.GuestRepository;
import com.raf.repository.UserRepository;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@Service
public class BookingServiceImpl implements BookingService {
    public static final BigDecimal DISCOUNT_THRESHOLD = new BigDecimal(1000);
    public static final BigDecimal DISCOUNT_MODIFIER = new BigDecimal(0.95);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private FlightRepository flightRepository;
    @Autowired
    private GuestRepository guestRepository;
    @Autowired
    private FlightReservationRepository flightReservationRepository;

    @Override
    public boolean discountQualifier(User user, Flight flight) {
        Validate.notNull(user);
        Validate.notNull(flight);
        if (user.getRoles().stream().filter(u -> RoleType.vip.equals(u.getName())).findFirst().isPresent()) {
            if (DISCOUNT_THRESHOLD.compareTo(flight.getPrice()) < 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean validateCanCancelFlightReservation(FlightReservation flightReservation, LocalDateTime nowDateTime) {
        return daysTillFlight(flightReservation, nowDateTime) >= 3L;
    }

    @Override
    public boolean validateCanUpdateFlightReservation(FlightReservation flightReservation, LocalDateTime nowDateTime) {
        return daysTillFlight(flightReservation, nowDateTime) >= 2L;
    }

    private long daysTillFlight(FlightReservation flightReservation, LocalDateTime nowDateTime) {
        final LocalDateTime flightDateTime = LocalDateTime.ofInstant(
                flightReservation.getFlight().getFlyDate().toInstant(), ZoneId.systemDefault());
        return ChronoUnit.DAYS.between(nowDateTime,flightDateTime);
    }

    @Override
    public Object bookFlight(BookingForm bookingForm, Date reservationDate) {
        Validate.notNull(bookingForm);
        Flight flight = flightRepository.findOne(bookingForm.getFlightId());
        if (flight == null) {
            throw new IllegalStateException("Missing flight id: " + bookingForm.getFlightId());
        }
        if (bookingForm.getUserId() != null) {
            return handleUserBooking(bookingForm, flight, reservationDate);
        } else {
            return handleGuestBooking(bookingForm, flight, reservationDate);
        }
    }

    public Object handleGuestBooking(BookingForm bookingForm, Flight flight, Date reservationDate) {
        if (validateGuestData(bookingForm)) {
            Guest guest = guestRepository.saveAndFlush(new Guest(bookingForm));
            return bookForGuest(guest, flight, bookingForm.getBags(), reservationDate);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    public FlightReservation handleUserBooking(BookingForm bookingForm, Flight flight, Date reservationDate) {
        User user = userRepository.findOne(bookingForm.getUserId());
        if (user == null) {
            throw new IllegalStateException("Missing user id: " + bookingForm.getUserId());
        }
        return bookForUser(user, flight, bookingForm.getBags(), reservationDate);
    }

    private boolean validateGuestData(BookingForm bookingForm) {
        if (bookingForm.getEmail() != null && bookingForm.getPesel() != null && bookingForm.getUsername() != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public FlightReservation bookForGuest(Guest guest, Flight flight, int bags, Date reservationDate) {
        FlightReservation flightReservation = new FlightReservation();
        fillCommonFields(flight, bags, reservationDate, flightReservation);
        flightReservation.setGuest(guest);
        flightReservation.setTotalPrice(flight.getPrice());
        return flightReservationRepository.saveAndFlush(flightReservation);
    }

    @Override
    public FlightReservation bookForUser(User user, Flight flight, int bags, Date reservationDate) {
        FlightReservation flightReservation = new FlightReservation();
        fillCommonFields(flight, bags, reservationDate, flightReservation);
        flightReservation.setUser(user);
        BigDecimal totalPrice = flight.getPrice();
        if (discountQualifier(user, flight)) {
            totalPrice = totalPrice.multiply(DISCOUNT_MODIFIER);
        }
        flightReservation.setTotalPrice(totalPrice);
        return flightReservationRepository.saveAndFlush(flightReservation);
    }

    public void fillCommonFields(Flight flight, int bags, Date reservationDate, FlightReservation flightReservation) {
        flightReservation.setFlight(flight);
        flightReservation.setBags(bags);
        flightReservation.setPaid(false);
        flightReservation.setReservationDate(reservationDate);
    }
}
