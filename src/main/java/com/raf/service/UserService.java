package com.raf.service;

import com.raf.model.User;

public interface UserService {
    User createUserAccount(User user);
}
