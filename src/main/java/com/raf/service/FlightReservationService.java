package com.raf.service;

import com.raf.model.FlightReservation;

import java.time.LocalDateTime;
import java.util.Date;


public interface FlightReservationService {
    boolean cancelReservation(long id, LocalDateTime thresholdDate);
    FlightReservation modifyReservation(long id, LocalDateTime thresholdDate, int bags,boolean paid);
    long deleteUnpaidFlightReservations(Date thresholdDate);
}
