INSERT INTO Role(id,name) VALUES (1,'VIP');
INSERT INTO Role(id,name) VALUES (2,'REGULAR');

INSERT INTO User(id,username,password,pesel,email) VALUES (1,'test1','test1','1234','test1@test.pl');
INSERT INTO User(id,username,password,pesel,email) VALUES (2,'test2','test2','1234','test2@test.pl');

INSERT INTO user_role(user_id,role_id) VALUES(1,1);
INSERT INTO user_role(user_id,role_id) VALUES(2,2);

INSERT INTO Flight(id,fly_date,total_seats,price,from_city,to_city) VALUES(1,'2018-10-03 00:00:00',132,1001,'WRO','WAW');
INSERT INTO Flight(id,fly_date,total_seats,price,from_city,to_city) VALUES(2,'2017-10-04 00:00:00',138,1000,'WAW','WRO');