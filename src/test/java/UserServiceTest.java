import com.raf.model.Role;
import com.raf.model.RoleType;
import com.raf.model.User;
import com.raf.repository.RoleRepository;
import com.raf.repository.UserRepository;
import com.raf.service.UserService;
import com.raf.service.UserServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    private RoleRepository roleRepository;
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private static UserService userService = new UserServiceImpl();

    @Test
    public void testCreateUserAccountWithVipRole() {
        User user = new User();
        user.setVip(true);

        Role role = new Role();
        role.setName(RoleType.vip);

        Mockito.when(roleRepository.findByName(RoleType.vip)).thenReturn(role);

        userService.createUserAccount(user);
        verify(roleRepository, times(1)).findByName(any());
    }
}
