import com.raf.SpringBootWebApplication;
import com.raf.model.Flight;
import com.raf.model.FlightReservation;
import com.raf.model.Role;
import com.raf.model.User;
import com.raf.repository.FlightRepository;
import com.raf.repository.UserRepository;
import com.raf.service.BookingService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static com.raf.model.RoleType.regular;
import static com.raf.model.RoleType.vip;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {BookingServiceTest.class, SpringBootWebApplication.class})
@WebAppConfiguration
public class BookingServiceTest {
    @Autowired
    private BookingService bookingService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private FlightRepository flightRepository;

    @Test
    public void testValidateCanCancelFlightReservation() {
        LocalDateTime flyTime = LocalDateTime.now();
        LocalDateTime days2 = flyTime.minusDays(2);
        LocalDateTime days3 = flyTime.minusDays(3);
        LocalDateTime days4 = flyTime.minusDays(4);
        FlightReservation flightReservation = new FlightReservation();
        Flight flight = new Flight();
        flight.setFlyDate(Date.from(flyTime.atZone(ZoneId.systemDefault()).toInstant()));
        flightReservation.setFlight(flight);

        Assert.assertFalse(bookingService.validateCanCancelFlightReservation(flightReservation, days2));
        Assert.assertTrue(bookingService.validateCanCancelFlightReservation(flightReservation, days3));
        Assert.assertTrue(bookingService.validateCanCancelFlightReservation(flightReservation, days4));
    }

    @Test
    public void testValidateCanUpdateFlightReservation() {
        LocalDateTime flyTime = LocalDateTime.now();
        LocalDateTime days2 = flyTime.minusDays(1);
        LocalDateTime days3 = flyTime.minusDays(2);
        LocalDateTime days4 = flyTime.minusDays(3);
        FlightReservation flightReservation = new FlightReservation();
        Flight flight = new Flight();
        flight.setFlyDate(Date.from(flyTime.atZone(ZoneId.systemDefault()).toInstant()));
        flightReservation.setFlight(flight);

        Assert.assertFalse(bookingService.validateCanUpdateFlightReservation(flightReservation, days2));
        Assert.assertTrue(bookingService.validateCanUpdateFlightReservation(flightReservation, days3));
        Assert.assertTrue(bookingService.validateCanUpdateFlightReservation(flightReservation, days4));
    }

    @Test
    public void testDiscountQualifier() {
        User user1 = new User();
        Role vipRole = new Role();
        vipRole.setName(vip);
        user1.getRoles().add(vipRole);

        User user2 = new User();
        Role role = new Role();
        role.setName(regular);
        user2.getRoles().add(role);

        Flight flight1 = new Flight();
        flight1.setPrice(new BigDecimal(1000));

        Flight flight2 = new Flight();
        flight2.setPrice(new BigDecimal(1001));

        Assert.assertFalse(bookingService.discountQualifier(user1, flight1));
        Assert.assertTrue(bookingService.discountQualifier(user1, flight2));
        Assert.assertFalse(bookingService.discountQualifier(user2, flight1));
        Assert.assertFalse(bookingService.discountQualifier(user2, flight2));
    }

    @Test
    public void testBookForUser() {
        User user1 = userRepository.findOne(1L);
        Flight flight2=flightRepository.findOne(1L);

        FlightReservation flightReservation = bookingService.bookForUser(user1, flight2, 1, new Date());
        Assert.assertTrue(flightReservation.getTotalPrice().compareTo(flight2.getPrice()) < 0);
    }

}
